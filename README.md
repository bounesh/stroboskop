# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

((git init
git remote add origin https://bounesh@bitbucket.org/bounesh/stroboskop.git
git pull origin master))

Naloga 6.2.3:
https://bitbucket.org/bounesh/stroboskop/commits/738c5f74ae3903a75e7f210782f674f25a719138

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/bounesh/stroboskop/commits/093f5370a0bce23f2b72fe5e3189c5e48fc67d68

Naloga 6.3.2:
https://bitbucket.org/bounesh/stroboskop/commits/4c49e3438de3129d6fcfbc6dfbdfe015edcc9e81

Naloga 6.3.3:
https://bitbucket.org/bounesh/stroboskop/commits/2dce7a5d802715f69b631c99342dbbb5e50e0759

Naloga 6.3.4:
https://bitbucket.org/bounesh/stroboskop/commits/fae80c43424f02255439549f181c5cddc602dc36

Naloga 6.3.5:

((git checkout master
git merge izgled
git push origin master))

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/bounesh/stroboskop/commits/61b9011d0ff2a87fbed561b9dc9c607b3bbd93c9

Naloga 6.4.2:
https://bitbucket.org/bounesh/stroboskop/commits/1a6e48459ec9282a9031955572efacf2b6af9067

Naloga 6.4.3:
https://bitbucket.org/bounesh/stroboskop/commits/bd5577e5dfe32f1cdd67ca630227768da5322e5a

Naloga 6.4.4:
https://bitbucket.org/bounesh/stroboskop/commits/643d9a8fabb8bdad1f1637f9e013446405e2afef